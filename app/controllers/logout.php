// Author:  AlexHG @ XENOBYTE.XYZ
// License: MIT License
// Website: https://XENOBYTE.XYZ


// This file doesn't do anything but validate logout requests by having an existing
// file for the HTTPRequest class to properly recognize the request as valid.
